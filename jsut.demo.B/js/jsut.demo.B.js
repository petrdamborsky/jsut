﻿function loadData(successAction) {
    $.ajax({
        dataType: 'json',
        url: '/data/data.json',
        success: function (response) {
            action(response);
        },
        error: function (response) {
            console.log(response);
        }
    });
}

function action(data) {
    $.each(data, function (i, item) {
        document.write(item);
    });
}